module.exports = {
  presets: ['@babel/react'],
  plugins: [
    '@babel/transform-runtime',
    ['@babel/proposal-decorators', { legacy: true }],
    ['@babel/proposal-class-properties', { loose: true }],
    '@babel/proposal-export-default-from',
    '@babel/proposal-export-namespace-from',
    '@babel/proposal-object-rest-spread'
  ]
};
