const electron = require('electron');
const { BrowserWindow, app, ipcMain } = electron;

const RPC = require('discord-rpc');
const client = new RPC.Client({
  transport: 'ipc'
});

client.on('ready', () => {
  console.log('Discord RPC ready');
  client.setActivity({
    state: 'Connected',
    largeImageKey: 'icon',
    startTimestamp: new Date()
  });
});

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      backgroundThrottling: false,
      devTools: true
    },
    show: false,
    focusable: true,
    fullscreenable: true
  });
  win.loadURL('http://localhost:8080');
  win.on('ready-to-show', () => {
    win.show();
  });
  win.on('show', () => {
    win.maximize();
    win.setAlwaysOnTop(true);
    win.focus();
    win.setAlwaysOnTop(false);
  });
  win.on('close', () => {
    win = null;
    app.exit();
  });
}

app.on('ready', async () => {
  await client.connect('511590542627569694');
  let data = await client.request('AUTHORIZE', {
    client_id: '511590542627569694',
    scopes: ['rpc', 'rpc.api', 'identify']
  });
  global.data = data;
  createWindow();
});

ipcMain;

ipcMain.on('ws.access', async (event, token) => {
  await client.login({
    clientId: '511590542627569694',
    accessToken: token,
    scopes: ['rpc', 'rpc.api', 'identify']
  });
});
