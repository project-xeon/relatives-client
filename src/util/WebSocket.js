import EventEmitter from 'eventemitter3';
export default class XeonWebSocket extends EventEmitter {
  constructor(url) {
    super();
    this.ws = new WebSocket(url);
    this.ws.onopen = this.onOpen.bind(this);
    this.ws.onclose = this.onClose.bind(this);
    this.ws.onmessage = this.onMessage.bind(this);
    this.ws.onerror = this.onError.bind(this);
  }
  send(op, args) {
    this.ws.send(
      JSON.stringify({
        op,
        ts: Date.now(),
        data: args
      })
    );
  }

  onOpen(...args) {
    this.emit('open', ...args);
  }
  onClose(...args) {
    this.emit('close', ...args);
  }
  onMessage(data) {
    try {
      let msg = JSON.parse(data.data);
      let newMsg = { ...msg, op: undefined };
      this.emit(msg.op, newMsg);
      //this.emit('message', ...args);
    } catch (err) {}
  }
  onError(...args) {
    this.emit('_error', ...args);
  }
}
