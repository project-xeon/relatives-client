export default class TerminalAPI {
  static queue = [];
  static interval;
  static delayedWrite(string) {
    this.write(string, true);
  }
  static write(string, delayed = false) {
    if (global.terminalInstance) {
      global.terminalInstance.write(string, {
        delayed
      });
    } else {
      this.queue.push([string, { delayed }]);
      if (!this.interval) {
        this.interval = setInterval(() => {
          if (!global.terminalInstance) return;
          clearInterval(this.interval);
          global.terminalInstance.writeQueue(this.queue);
          this.queue = [];
        }, 50);
      }
    }
  }
  static clear() {
    if (global.terminalInstance) {
      global.terminalInstance.clear();
    }
  }
}
