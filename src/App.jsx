import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Terminal from './components/Terminal';
import styles from './styles/index.scss';
import XeonWebSocket from './util/WebSocket';
import { remote, ipcRenderer } from 'electron';

//const WS_URL = 'ws://us.logon.mudjs.net:13370';
const WS_URL = 'ws://xeondev.mudjs.net:13370';
const TEST_MODE = true;
const TEST_OBJ = {
  username: 'relative',
  id: '346676896794279937'
};
export default class App extends PureComponent {
  state = {
    displayPrompt: false
  };
  componentDidMount() {
    this.ws = new XeonWebSocket(WS_URL);
    global.xws = this.ws;
    const code = remote.getGlobal('data').code;
    this.ws.on('open', () => {
      Terminal.API.delayedWrite('Connection established...');
      Terminal.API.delayedWrite('Authenticating...');
      if (TEST_MODE) {
        this.ws.send('auth', TEST_OBJ);
      } else {
        this.ws.send('auth', {
          code
        });
      }
    });
    this.ws.once('access', token => {
      ipcRenderer.send('ws.access', token);
    });
    this.ws.once('auth', dobj => {
      Terminal.API.delayedWrite('Authenticated!');
      this.setState({
        displayPrompt: true
      });
    });
  }

  executeCommand = command => {
    console.log(command);
  };
  render() {
    return (
      <div className={styles.appRoot}>
        <Provider store={store}>
          <div className={styles.appContainer}>
            <Terminal
              displayPrompt={this.state.displayPrompt}
              status={'idle'}
            />
            <div className={styles.appBorder} />
          </div>
        </Provider>
      </div>
    );
  }
}
