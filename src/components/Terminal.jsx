import React, { PureComponent } from 'react';
import { Colors } from '../constants';
import TerminalAPI from '../util/TerminalAPI';
import store from '../store';
import * as actions from '../actions';
import classNames from 'classnames';
import { connect } from 'react-redux';
import styles from '../styles/Terminal.scss';
import { System } from '../os';
Math.randomInRange = function(max = 1, min = 0) {
  return Math.round(Math.random() * (max - min) + min);
};

Array.prototype.random = function() {
  return this[Math.randomInRange(this.length - 1)];
};

const decodeEntities = (function() {
  // this prevents any overhead from creating the object each time
  var element = document.createElement('div');

  function decodeHTMLEntities(str) {
    if (str && typeof str === 'string') {
      // strip script/html tags
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gim, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gim, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }

    return str;
  }

  return decodeHTMLEntities;
})();

const createElementFromCode = (str, color) => {
  let newColor = color;
  if (newColor === '?') {
    newColor = Colors[Object.keys(Colors).random()];
  } else {
    newColor = Colors[color] || Colors['*'];
  }
  return (
    <span
      className="glow"
      style={{
        color: newColor,
        textShadow: `0 0 7px ${newColor}`
      }}
    >
      {decodeEntities(str)}
    </span>
  );
};

const createElementFromStringWithColor = str => {
  let sub = str.substr(0, 1);
  let end = str.substr(1);
  if (typeof Colors[sub] !== 'undefined' || sub === '?') {
    return createElementFromCode(end, sub);
  }
  return createElementFromCode(end, '*');
};

const createElementFromString = str => {
  let splitStr = str.split('¬');
  if (splitStr.length === 1)
    return <span className={styles.terminalLine}>{decodeEntities(str)}</span>;
  if (splitStr.length > 1) splitStr = splitStr.slice(1);
  return (
    <span className={styles.terminalLine}>
      {splitStr.map(spl => createElementFromStringWithColor(spl))}
    </span>
  );
};

const getNextCharacter = (text, lastPos) => {
  let newPos = lastPos;
  let textToRender = text.substr(0, newPos + 1);
  newPos++;
  if (textToRender.substr(textToRender.length - 1, 1) === '¬') {
    textToRender = text.substr(0, newPos + 1);
    newPos++;
  }
  return {
    textToRender,
    newPos
  };
};

class TerminalLine extends PureComponent {
  state = {
    rendered: false,
    colored: null,
    fullText: '',
    textToRender: '',
    lastPos: 0
  };
  interval = null;
  componentDidMount() {
    const { children, delayed } = this.props;
    const text = (typeof children !== 'undefined' && children[0] === 'string'
      ? children[0]
      : this.props.text
    ).replace(/&not;/g, '¬');
    this.state.fullText = text;
    if (delayed) {
      let { textToRender, newPos } = getNextCharacter(text, this.state.lastPos);
      this.state.lastPos = newPos;
      this.state.textToRender = textToRender;
      this.interval = setInterval(this.updateText.bind(this), 5);
    } else {
      this.state.textToRender = text;
    }
  }
  updateText() {
    let text = this.state.fullText;
    let { textToRender, newPos } = getNextCharacter(text, this.state.lastPos);
    if (newPos === text.length || newPos > text.length) {
      clearInterval(this.interval);
      delete this.interval;
    }
    this.setState({
      lastPos: newPos,
      textToRender
    });
  }
  render() {
    let text = this.state.textToRender;
    let colored = createElementFromString(text);
    return colored;
  }
}
@connect(
  state => ({
    messages: state.messages,
    user: state.user,
    terminal: state.terminal
  }),
  actions
)
export default class Terminal extends PureComponent {
  constructor() {
    super();
    global.terminalInstance = this;
  }
  static Line = TerminalLine;
  static API = TerminalAPI;

  state = {
    user: 'relative',
    terminal: 'zeta-0526'
  };
  clear() {
    this.props.terminalClear();
  }
  write(str, options) {
    options = { delayed: false, ...options };
    let newEl = <Terminal.Line delayed={options.delayed} text={str} />;
    this.props.terminalWrite(newEl);
  }
  writeQueue(queue) {
    queue.forEach(queueObj => {
      let str = queueObj[0];
      let options = queueObj[1];
      this.props.terminalWrite(
        <Terminal.Line delayed={options.delayed} text={str} />
      );
    });
  }

  inputKeyPress = e => {
    if (e.charCode === 13) {
      let val = e.target.value;
      if (e.target.value.length > 0) {
        e.target.value = '';
        this.write(`${System.prompt}${val}`, { delayed: true });
        System.executeCommand(val);
      }
    }
  };

  render() {
    const { messages, displayPrompt, user, terminal, status } = this.props;
    //let prompt = `${user}@${terminal} ► `;
    let prompt = System.prompt;
    if (status === 'travel') {
      prompt = 'Travelling...';
    }
    return (
      <div className={classNames(styles.terminalRoot, styles.crt)}>
        <pre className={styles.terminalOutput}>{messages}</pre>
        {displayPrompt && (
          <div className={styles.terminalInput}>
            <span className={styles.terminalPrompt}>
              {createElementFromString(prompt)}
            </span>
            {status === 'idle' && (
              <input
                type="text"
                className={classNames(styles.terminalInputEl, styles.crt)}
                style={{ width: '563px' }}
                onKeyPress={this.inputKeyPress}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}
