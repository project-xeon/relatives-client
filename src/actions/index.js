import {
  TERMINAL_WRITE,
  TERMINAL_CLEAR,
  SET_USER,
  SET_TERMINAL
} from '../constants/actionTypes';

export const terminalWrite = message => ({
  type: TERMINAL_WRITE,
  payload: message
});
export const terminalClear = () => ({
  type: TERMINAL_CLEAR
});
export const setUser = user => ({
  type: SET_USER,
  payload: user
});
export const setTerminal = terminal => ({
  type: SET_TERMINAL,
  payload: terminal
});
