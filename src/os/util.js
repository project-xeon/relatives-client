export const escapeArg = arg => {
  let argWrap = `${arg}`;
  if (typeof arg === 'string') {
    argWrap = `'${arg}'`;
  }
  return argWrap;
};
export const createFunctionCall = (fn, ...args) => {
  let argStr = args.map(escapeArg).join(',');
  return `return await ${fn}(${argStr});`;
};

export const createVariableGet = variable => {
  return `return ${variable};`;
};

export const createVariableSet = (variable, value) => {
  return `${variable}=${escapeArg(value)};`;
};
