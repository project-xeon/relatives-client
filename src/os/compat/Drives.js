import { createFunctionCall } from '../util';
class Drives {
  stats(path, drive) {
    return global.system.eval(createFunctionCall('drives.stats', path, drive));
  }
  read(path, drive) {
    return global.system.eval(createFunctionCall('drives.read', path, drive));
  }
  write(path, data, drive) {
    return global.system.eval(
      createFunctionCall('drives.write', path, data, drive)
    );
  }
  get(drive) {
    return global.system.eval(createFunctionCall('drives.get', drive));
  }
  append(path, data, drive) {
    return global.system.eval(
      createFunctionCall('drives.append', path, data, drive)
    );
  }
  rm(path, recursive = false, drive) {
    return global.system.eval(
      createFunctionCall('drives.rm', path, recursive, drive)
    );
  }
  format(drive) {
    return global.system.eval(createFunctionCall('drives.format', drive));
  }
  mkdir(path, recursive = false, drive) {
    return global.system.eval(
      createFunctionCall('drives.mkdir', path, recursive, drive)
    );
  }
  readdir(path, drive) {
    return global.system.eval(
      createFunctionCall('drives.readdir', path, drive)
    );
  }
  exists(path, drive) {
    return global.system.eval(createFunctionCall('drives.exists', path, drive));
  }
  list() {
    return global.system.eval(createFunctionCall('drives.list'));
  }
}
export default new Drives();
