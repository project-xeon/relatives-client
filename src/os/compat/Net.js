import {
  createFunctionCall,
  createVariableGet,
  createVariableSet
} from '../util';
class Net {
  get proxy() {
    return () => global.system.eval(createVariableGet('net.proxy'));
  }
  set proxy(val) {
    global.system.eval(createVariableSet('net.proxy', val));
  }

  send(destination, port, data) {
    return global.system.eval(
      createFunctionCall('net.send', destination, port, data)
    );
  }
}
export default new Net();
