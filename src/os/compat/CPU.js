import { createFunctionCall } from '../util';
class CPU {
  get(slot) {
    return global.system.eval(createFunctionCall('cpu.get', slot));
  }
}
export default new CPU();
