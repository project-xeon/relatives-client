import ListCommand from './commands/ListCommand';
import EvalCommand from './commands/EvalCommand';
class CommandManager {
  commands = [];

  constructor() {
    this.register(new ListCommand());
    this.register(new EvalCommand());
  }

  register(command) {
    this.commands.push(command);
  }

  findCommand(command) {
    return this.commands.find(
      cmd => cmd.name === command || cmd.aliases.indexOf(command) !== -1
    );
  }
}
export default new CommandManager();
