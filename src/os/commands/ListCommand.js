import Command from '../Command';
import system from '../System';
export default class ListCommand extends Command {
  constructor() {
    super('list', {
      aliases: ['ls']
    });
  }
  async run(args) {
    let path = args[0] || '/';
    let dir = await system.drives.readdir(path);
    let drive = await system.drives.get();
    let data = dir.map(d => {
      let p = path + d;
      return {
        filename: d,
        directory: drive[p] === null
      };
    });
    let strSplit = data.map(fd => {
      return `¬${fd.directory ? 'B' : 'g'}${fd.filename}`;
    });
    return strSplit.join(' ');
  }
}
