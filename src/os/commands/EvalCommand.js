import Command from '../Command';
import system from '../System';
export default class EvalCommand extends Command {
  constructor() {
    super('eval', {
      aliases: [':']
    });
  }
  async run(args) {
    const data = await system.eval(args.join(' '));
    return JSON.stringify(data, null, 4);
  }
}
