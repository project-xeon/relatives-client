import { CommandManager } from './';
import Terminal from '../components/Terminal';
import { CPU, Drives, Net } from './compat';
class System {
  inSsh = false;
  user = 'root';
  machine = 'localhost';

  cpu = CPU;
  drives = Drives;
  net = Net;
  get ws() {
    return global.xws;
  }

  get prompt() {
    return `¬1${this.user}@${this.machine}:¬B¬W# `;
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return (
      s4() +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      s4() +
      s4()
    );
  }

  write(text) {
    Terminal.API.delayedWrite(text);
  }

  error(text) {
    return this.write(`¬RError: ¬r${text}`);
  }

  eval(code) {
    return new Promise((resolve, reject) => {
      const guid = this.guid();
      const wrapper = `(async() => {
        try {
          const response = await (async() => {
            ${code}
          })()
          return {
            error: false,
            res: response,
            id: '${guid}'
          }
        } catch(err) {
          return {
            error: true,
            res: err,
            id: '${guid}'
          }
        } 
      })()`;
      const evalCallback = args => {
        try {
          let data = JSON.parse(args.data);
          if (data.id === guid) {
            if (!data.error) {
              resolve(data.res);
            } else {
              reject(data.res);
            }
            this.ws.removeListener('eval', evalCallback);
          }
        } catch (err) {}
      };
      this.ws.on('eval', evalCallback);
      this.ws.send('eval', wrapper);
    });
  }

  async executeCommand(text) {
    let argsRaw = text.split(' ');
    let command = CommandManager.findCommand(argsRaw[0]);
    if (!command) {
      return this.error('Command not found');
    }
    let args = argsRaw.slice(1);
    try {
      let resp = await command.run(args);
      return this.write(resp);
    } catch (err) {
      return this.error(err.toString());
    }
  }
}
const system = new System();
global.system = system;
export default system;
