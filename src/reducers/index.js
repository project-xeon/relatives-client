import {
  TERMINAL_WRITE,
  TERMINAL_CLEAR,
  SET_USER,
  SET_TERMINAL
} from '../constants/actionTypes';

const initialState = {
  messages: [],
  user: '',
  terminal: ''
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case TERMINAL_WRITE:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      };
    case TERMINAL_CLEAR:
      return {
        ...state,
        messages: []
      };
    case SET_USER:
      return {
        ...state,
        user: action.payload
      };
    case SET_TERMINAL:
      return {
        ...state,
        terminal: action.payload
      };
    default:
      return state;
  }
};
export default rootReducer;
