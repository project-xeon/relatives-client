export const TERMINAL_WRITE = 'TERMINAL_WRITE';
export const TERMINAL_CLEAR = 'TERMINAL_CLEAR';
export const SET_USER = 'SET_USER';
export const SET_TERMINAL = 'SET_TERMINAL';
